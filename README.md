# Barbershop "Borodinski"

 * https://playua20.github.io/Borodinski-responsive/ - home
 * https://playua20.github.io/Borodinski-responsive/form.html - order
 * https://playua20.github.io/Borodinski-responsive/works.html - works
 
 > Устаревший free домен (2017): 
 
 >* [http://borodinski.kl.com.ua/](http://borodinski.kl.com.ua/) - Главная
 
 >* [http://borodinski.kl.com.ua/form.html](http://borodinski.kl.com.ua/form.html) - Медиа
 
 >* [http://borodinski.kl.com.ua/works.html](http://borodinski.kl.com.ua/works.html) - рабочая feedback форма




## Особенности:
 * Responsive
 * Adaptive images
 * Mobile first     
 * BEM
 
## Технологии:
 * jQuery
 * Flexbox
 * Gulp
 * Less

>*Дизайн шаблона принадлежит ***HtmlAcademy****

@ 2017-2023